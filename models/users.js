var mongoose = require("mongoose");


//create schema for user
var userSchema = new mongoose.Schema({
    phoneId:  String,
    socketId: String,
    aboutHimself : String,
    talkAbout : String,
    yourSecret : String,
    sex : Number,
    age : Number,
    lookingNow : Boolean,    //true,если проводится поиск собеседника
    geo: {type: [Number], index: '2d'}
});

userSchema.methods.findNearWithDistance = function(maxDist,callback) {
    maxDist /= 6371;
    return this.model('users').find({geo: { $nearSphere: this.geo, $maxDistance: maxDist},lookingNow:true }, callback);
};

//compile schema to model
module.exports.userSchema = userSchema;
module.exports = mongoose.model('users', userSchema);