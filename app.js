var express = require('express'),
     bodyParser = require('body-parser'),
     app = express(),
     nconf = require('nconf'),
     mongoose = require('mongoose'),
     path = require('path'),
     io = require('socket.io').listen(app.Server),
     anotherUser = require("./models/users");



nconf.argv()
    .env()
    .file({file: path.join(__dirname, "config","config.json")});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


mongoose.connect('mongodb://127.0.0.1:27017/users');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("zaebis")
});




io.use(function(socket, next){
    var query = socket.handshake.query;
    if((typeof query.phoneId) == "undefined") next(new Error('Authentication error'));
    if((typeof socket.id) == "undefined") next(new Error('Authentication error'));
    var user = new anotherUser({
        phoneId : query.phoneId,
        socketId : socket.id,    //внимательней к этому дерьму
        geo : {
            type : [query.lon,query.lat]
        },
        aboutHimself : query.aboutHimself,
        yourSecret : query.yourSecret,
        sex : query.yourSecret,
        talkAbout : query.talkAbout,
        age : query.age
    });
    user.save(function (err) {
        if (err) {
            console.log(err);
            new Error('Authentication error');
            return err;
        }
        else {
            console.log("Post saved");
        }
    });
    next();
});

var handleError = function (err) {
    console.log(err);
};


io.on('connection', function(socket){
    console.log('a user connected');
    socket.on("findCompanion",function () {
       findCompanionByDistance(socket.id);
    });

    socket.on("disconnect",function () {          // TODO проверить, как быстро будет приходить disconnect при потере инета
        anotherUser.userSchema.findOne({ 'socketId': socket.id }, function (err, user) {
            if (err) return handleError(err);             // TODO продумать обработку и логгинг(либу найти) ошибок
            user.remove();
        })
    })
});


var findCompanionByDistance = function (socketId) {
    var kilometersDistance = 4500;
    anotherUser.userSchema.findOne({ 'socketId': socketId }, function (err, user) {
        if (err) return handleError(err);             // TODO продумать обработку и логгинг(либу найти) ошибок
        var companionFounded = function (err,user) {
            if (err) return handleError(err);
            socket.emit("companionFounded",user.socketId);
        };

        var findNearCallback = function (err,queryUsers) {          //TODO продумать и оптимизировать этот ад
            if (err) return handleError(err);
            if(queryUsers != null && typeof queryUsers != "undefined"){
                if(queryUsers.length != 0) {
                    companionFounded(null,queryUsers[0]);
                }
            }else {
                anotherUser.userSchema.findOne({lookingNow : true}, companionFounded(err,user));
            }
        };

        user.methods.findNearWithDistance(kilometersDistance,findNearCallback(err,queryUsers));
    });
};

app.get("/",function (req, res) {
    res.send("go to login");
   /* mongoose.model('unicorns').find(function (err , users) {
        res.send(users);
    })*/

});



var server = app.listen(3000,function () {
    console.log("Listening on port %s...", server.address().port);
});



/*

 app.get("/login",function (req, res) {
 console.log((typeof req.query.id) == "undefined");
 var user = new anotherUser({
 id : req.query.id,
 geo : {
 lat : req.query.lat,
 lon : req.query.lon
 },
 });
 user.save(function (err) {
 if (err) {
 console.log(err);
 return err;
 }
 else {
 console.log("Post saved");
 }
 });
 });


----Нахождение с постепенным увеличением дистанции----
 var findNearCallback = function (err,queryUsers) {          //TODO продумать и оптимизировать этот ад
 if (err) return handleError(err);
 if(kilometersDistance > 4000)anotherUser.userSchema.findOne({lookingNow : true}, companionFounded());
 if(queryUsers != null && typeof queryUsers != "undefined"){
 if(queryUsers.length != 0) {
 var coupleUser;
 for (var i in queryUsers) {
 if(i.lookingNow){
 coupleUser = i;
 break;
 }
 }
 if(typeof coupleUser == "undefined")user.methods.findNearWithDistance(kilometersDistance*2,findNearCallback(err,queryUsers));
 else companionFounded(null,user);
 }
 }else {
 user.methods.findNearWithDistance(kilometersDistance*2,findNearCallback(err,queryUsers))
 }
 };
 */

